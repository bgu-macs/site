---
title: "Homework"
anchor: homework
date: 2021-03-02T14:37:41+02:00
draft: false
weight: 20
---

Assignments must be submitted via Moodle. To submit the
assignment, download (_File → Download .ipynb_ in Google Colab)
the notebook, then upload the downloaded notebook to Moodle.

1. [Assignment 1: logarithm and exponent](https://colab.research.google.com/drive/1VjvyNwlKQ5Tr8rHXrNle6BY7UXVXFYE6), submission deadline **Oct 20th, 2021, 23:59**. _This assignment will not be graded_ --- use it to become familiar with the routine.
2. [Assignment 2: approximating mathematical functions](https://colab.research.google.com/drive/18JujL6LrwfMBUIDiizn6lR0zlynlyrdH) --- deadline **Nov 3rd, 2021, 23.59**.
3. [Assignment 3: applications of modular arithmetics](https://colab.research.google.com/drive/1-ly_UkSVqW3PhnaR4kX2d-dj7LqaN7VG) --- deadline **Nov 17th, 2021. 23:59**.
4. [Assignment 4: matrices, eigenvectors, Google PageRank](https://colab.research.google.com/drive/1T6ZPpQYi5UbugD8X0voofCCFvbeSzsoo) --- deadline **Dec 1st, 2021, 23:59**.
5. [Assignment 5: differentiation and optimization](https://colab.research.google.com/drive/1KwFt9Xvb-L_PI5XU68iHJS_gwVGMsIHC) --- deadline **Dec 18th, 2021, 23:59**.
6. [Assignment 6: Fourier transform]() --- deadline **Jan 1st, 2022, 23:59**.
