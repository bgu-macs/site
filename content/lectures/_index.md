---
title: "Lectures"
anchor: lectures
date: 2021-03-02T14:37:41+02:00
draft: false
weight: 10
---

1. [Course organization, online resources, logarithm and exponent](slides/01intro-logexp.html); [recording](https://youtu.be/bVHMWIhd22U); [notebook](https://colab.research.google.com/drive/1PS2XZEKAhsV7Wq97Szk1RH93HWuhTyYF).
 > Introduction of course's resources and orgranization: website, Slack, Zoom, Google Colab, Python. Logarithm and exponent as mathematical functions, and their role in performing stable floating point computations in computer science.

2. [Taylor series, optimal polynomials, and Remez algorithm for approximate function computation](slides/02approximation.html); [recording](https://youtu.be/X6kFogoHcys); [notebook](https://colab.research.google.com/drive/1PAXmH_v6sQmbmAYrRK48eCfFQWLcQO3K).
> Computations involve obtaining values of library functions, such as exp, log, sin, cos, erf etc. These functions are not 'built-in' into the computer and must be approximated using primitive operations (+, -, \*, /). We will discuss options for polynomial approximations of functions using different approaches and see how functions are computed in the standard library of a computer language.

3. [Modular arithmetics, hashing, integrity checking, cryptography](slides/03modular.html); [recording](https://youtu.be/IxNhqjGEsuI); [notebook](https://colab.research.google.com/drive/1Byc11ZKz-Bd4BxWOh9YnkEJGPnUeAK1C).
> Modular arithmetics is a seemingly basic idea of returning both quotient and remainder as the results of integer division. Quite surprisingly, modular arithmetic has important applications in computer science. We will look at two applications: hashing and integrity checking.

4. [Eigenvalues and eigenvectors, Google PageRank algorithm](slides/04pagerank.html); [recording](https://youtu.be/6HSrjtugnDY); [notebook](https://colab.research.google.com/drive/123LRvDkKi_aaAua-BAnoqi0yOj9E-bAA).
> Linear algebra deals with linear equations and with related mathematical problems. Linear algebra operates on matrices and vectors, representing coefficients and unknowns in linear expressions. Square matrices have many interesting features, one of them is the existence of eigenvectors and eigenvalues. Despite pure mathematical uses, eigenvalues find many applications in computer science. We will explore one of them, Google PageRank, the original algorithm behind Google's revolutionary search engine.

5. [Differentiation, optimization, algorithmic differentiation](slides/05autodiff.html); [recording](https://youtu.be/n8j6UdxgQx8); [notebook](https://colab.research.google.com/drive/1MPDAqyAJgbaFA7TXrpnMF9JgogDdCOOk).
> Calculus is in the core of many scientific methods in natural sciences. Differential calculus is concerned with finding and computing derivatives of functions. Derivatives are important for efficient optimization algorithms. We will learn about algorithmic differentiation, a universal method for computing derivatives of program code, and see examples of its use in modern computation.

6. [Complex numbers, Fourier transform, signal processing](slides/06fourier.html); [recording](https://youtu.be/pWHZIeMI5ho); [notebook](https://colab.research.google.com/drive/1nHTgnocjHZ3BaoOo6rZg8hmqED1Gf3yP?usp=sharing).
> Complex numbers are an extension of real numbers, and take the form _a + ib_ where _i_, the imaginary number, satisfies _i² = -1_. Complex numbers have many exciting purely mathematical features. However, they also have wide practical uses in computer science, such as signal processing and noise reduction. We will discuss some of the uses.

