---
title: "Summary"
anchor: summary
date: 2021-03-02T14:37:41+02:00
draft: false
weight: 1
---

The course surveys applications of mathematics in various areas of computer
science. The course consists of six biweekly two-hour lectures. During each
lecture, we raise a problem in computer science, review a mathematical concept
relevant for a solution of the problem, and finally see how mathematics is used
to solve the problem in practice. There are five graded homework assignments. In
homework assignments, the students apply mathematical concepts reviewed in
class to computer science tasks. The assignments are implemented in
[Python](http://www.python.org), in [Google
Colab](http://colab.research.google.com).

## How we learn

We meet biweekly (every over week) for two hours. Assignments are published in
the [Homework](#homework) section. There will be 5 _graded_ homework assigments
(first assignment will not be graded).  Homework assignments are done
**individually**.

We use [Slack](https://bgu-macs.slack.com/) as the platform for questions and
discussions of class material and homework. [Sign up](https://join.slack.com/t/bgu-macs/signup)
with your **BGU e-mail address** (e.g. `almonip@post.bgu.ac.il`).
