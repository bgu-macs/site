--- 
title: "Links"
anchor: links
date: 2021-03-02T14:37:41+02:00
draft: false
weight: 40
---

* [Google Colab](http://colab.research.google.com) &mdash; Google online development environment, based on Jupyter Notebooks. No installation required.
* [Introduction to Python](https://colab.research.google.com/github/cs231n/cs231n.github.io/blob/master/python-colab.ipynb) &mdash; interactive Python tutorial, in Google Colab.
* [Slack space](http://bgu-macs.slack.com/) &mdash; slack workspace for the course. We will use Slack as the platform for questions and discussions of class material and homework ([sign-up link](https://join.slack.com/t/bgu-macs/signup)).

