<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <title>MACS: Approximating library functions</title>

        <link rel="stylesheet" href="css/reveal.css">
        <link rel="stylesheet" href="css/theme/white.css">

        <!-- Theme used for syntax highlighting of code -->
        <link rel="stylesheet" href="lib/css/arduino-light.css">

        <!-- Printing and PDF exports -->
        <script>
            var link = document.createElement( 'link' );
link.rel = 'stylesheet';
link.type = 'text/css';
link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
document.getElementsByTagName( 'head' )[0].appendChild( link );
        </script>
        <style type="text/css">
.reveal .slides section .static { visibility: hidden; height: 0; width: 0} /* replace with animated for PDF export */

.reveal .slides section .highlight-current-bg-cyan,
.reveal .slides section .highlight-current-bg-magenta,
.reveal .slides section .fragment.highlight-current-bg-yellow,
.reveal .slides section .highlight-bg-cyan,
.reveal .slides section .highlight-bg-magenta,
.reveal .slides section .fragment.highlight-bg-yellow {
    opacity: 1;
    visibility: inherit; }

        .reveal .slides section .highlight-bg-cyan.visible {
            background-color: cyan;
        }
        .reveal .slides section .highlight-bg-magenta.visible {
            background-color: magenta;
        }
        .reveal .slides section .fragment.highlight-bg-yellow.visible {
            background-color: yellow;
        }
        .reveal .slides section .highlight-current-bg-cyan.current-fragment {
            background-color: cyan;
        }
        .reveal .slides section .highlight-current-bg-magenta.current-fragment {
            background-color: magenta;
        }
        .reveal .slides section .fragment.highlight-current-bg-yellow.current-fragment {
            background-color: yellow;
        }
        </style>
    </head>
    <body>
        <div class="reveal">
            <div class="slides">
                <section>
                    <h2>Applications of Mathematics in Computer Science (MACS)</h2>
                    <hr/>
                    <h1 style="font-size: 120%">Approximating library functions</h1>
                    <p>Concepts:</p>
                    <ul>
                        <li>analytic functions;</li>
                        <li>Taylor series;</li>
                        <li>Remez algorithm.</li>
                    </ul>
                    <p style="margin-top: 1em"><emph>David Tolpin, <a href="mailto:david.tolpin@gmail.com">david.tolpin@gmail.com</a></p>
                </section>

                <section>
                    <section>
                        <h2>Library of mathematical functions</h2>
                        <p>
                        <a href="https://docs.python.org/3/library/math.html">https://docs.python.org/3/library/math.html</a>
                        </p>
                        <ul>
                            <li>Number-theoretic and representation functions</li>
                            <li><b>Power and logarithmic functions</b></li>
                            <li>Angular conversions</li>
                            <li><b>Trigonometric functions</b></li>
                            <li><b>Hyperbolic functions</b></li>
                            <li><b>Special functions</b></li>
                            <li>Constants</li>
                        </ul>
                    </section>
                    <section>
                        <h2>Uses of functions</h2>
                        <table>
                            <tr><td>power and logarithmic</td><td>efficient computation, data analysis</td></tr>
                            <tr><td>trigonometric</td><td><b>physics</b>, machine learning, <b>time series</b></td></tr>
                            <tr><td>hyperbolic</td><td><b>physics</b>, <b>machine learning</b>, time series</td></tr>
                            <tr><td>special functions</td><td><b>statistics</b>, machine learning</td></tr>
                        </table>
                    </section>

                    <section>
                        <h2>Usage examples</h2>
                        <ul>
                            <li><tt>log</tt> &mdash; <a href="https://github.com/jayelm/decisiontrees/blob/master/id3.py">decision trees</a> (machine learning)</li>
                            <li><tt>tanh</tt> &mdash; <a href="https://github.com/NadeemWard/pytorch_simple_policy_gradients/blob/master/reinforce/REINFORCE_continuous.py#L51">REINFORCE algorithm</a> (planning)</li>
                            <li><tt>erf</tt> &mdash; <a href="https://github.com/MaterialsDiscovery/PyChemia/blob/master/pychemia/utils/mathematics.py">materials discovery and design</a> (statistical modeling)</li>
                        </ul>
                    </section>
                    <section>
                        <h2>Computing mathematical functions</h2>
                        <p>Example: <a href="https://github.com/golang/go/blob/master/src/math/sin.go">sin and cos</a>, in Go.</p>
                        <pre><code language="Go" data-trim style="font-size: 70%">
var _cos = [...]float64{
    -1.13585365213876817300e-11, // 0xbda8fa49a0861a9b
    2.08757008419747316778e-9,   // 0x3e21ee9d7b4e3f05
    -2.75573141792967388112e-7,  // 0xbe927e4f7eac4bc6
    2.48015872888517045348e-5,   // 0x3efa01a019c844f5
    -1.38888888888730564116e-3,  // 0xbf56c16c16c14f91
    4.16666666666665929218e-2,   // 0x3fa555555555554b
}

func Cos(x float64) float64 {
    const (
        PI4A = 7.85398125648498535156e-1  // 0x3fe921fb40000000, Pi/4 split into three parts
        PI4B = 3.77489470793079817668e-8  // 0x3e64442d00000000,
        PI4C = 2.69515142907905952645e-15 // 0x3ce8469898cc5170,
    )
    // special cases
    switch {
    case IsNaN(x) || IsInf(x, 0):
        return NaN()
    }

    // make argument positive
    sign := false
    x = Abs(x)

    var j uint64
    var y, z float64
    if x >= reduceThreshold {
        j, z = trigReduce(x)
    } else {
        j = uint64(x * (4 / Pi)) // integer part of x/(Pi/4), as integer for tests on the phase angle
        y = float64(j)           // integer part of x/(Pi/4), as float

        // map zeros to origin
        if j&1 == 1 {
            j++
            y++
        }
        j &= 7                               // octant modulo 2Pi radians (360 degrees)
        z = ((x - y*PI4A) - y*PI4B) - y*PI4C // Extended precision modular arithmetic
    }

    if j > 3 {
        j -= 4
        sign = !sign
    }
    if j > 1 {
        sign = !sign
    }

    zz := z * z
    if j == 1 || j == 2 {
        y = z + z*zz*((((((_sin[0]*zz)+_sin[1])*zz+_sin[2])*zz+_sin[3])*zz+_sin[4])*zz+_sin[5])
    } else {
        y = 1.0 - 0.5*zz + zz*zz*((((((_cos[0]*zz)+_cos[1])*zz+_cos[2])*zz+_cos[3])*zz+_cos[4])*zz+_cos[5])
    }
    if sign {
        y = -y
    }
    return y
}
                        </code></pre>
                    </section>
                </section>

				<section>
					<section>
						<h2>Approximation theory</h2>
						<ul>
							<li>Polynomials</li>
							<li>Analytic functions</li>
							<li>Taylor series</li>
							<li>Remez algorithm</li>
						</ul>
					</section>
					<section>
						<h2>Polynomials</h2>
						<ul>
						<li>Polynomial: $p(x) = \sum_{i=0}^N a_i x^i$</li>
							<li>Polynomials ar easy to compute:
								<pre><code language="python" data-trim>
                                def poly(x, aa):
                                    xi, v = 1, 0
                                    for a in aa:
                                        v += a*xi
                                        xi *= x
                                    return v
								</code></pre>
							</li>
							<li>Sum and product of polynomials are polynomials</li>
							<li><i>Ratios</i> of polynomials are also easy to compute</li>
							<li>$N=\infty$ &Rightarrow; <i>power series:</i> $\sum_{i=0}^\infty a_i x^i$</li>

						</ul>
					</section>
					<section>
						<h2>Analytic functions</h2>
						
						<p>Function $f(x)$ is <i>analytic</i> if
						for any $x_0 \in D$:
						$$f(x) = \sum_{i=0}^\infty a_i (x-x_0)^i$$. </p>
						<ul>
							<li>$f(x)$ is analytic if can be computed as a power series.</li>
							<li>Polynomials are analytic.</li>
							<li>We can <i>approximate</i> $f(x) \approx \sum_{i=0}^N a_i (x-x_0)^i$,
								because if $|x-x_0|<1$, $\lim_{n\to\infty} (x-x_0)^n = 0$.
							</li>
						</ul>
					</section>

					<section>
						<h2>Examples of analytic functions</h2>
						<ul>
							<li>$\exp(x) = \sum_{i=0}^\infty \frac {x^i} {i!}$</li>
							<li>$\ln (x) = \sum_{i=1}^\infty (-1)^{i+1}\frac{(x-1)^i}{i}$ for $0 < x < 2$</li>
							<li>$\sin(x) = \sum_{i=0}^\infty \frac {(-1)^i} {(2i+1)!}x^{2i+1}$</li>
						</ul>
						<p>These are <b>Taylor series!</b></p>
					</section>

					<section>
						<h2>Taylor series</h2>
						<p>If $f(x)$ is <i>infinitely differentiable</i>, then 
						$$\mathrm{Taylor}(f(x), a) = \sum_{i=0}^\infty \frac {f^{(i)}(a)} {i!} (x-a)^i$$
						at point $a$.</p>
						<ul>
							<li>Taylor series of a polynomial is the polynomial.</li>
							<li>For most functions, $f(x) = \mathrm{Taylor}(f(x), a)$ for <i>some</i> $a$.</li>
							<li>If $f(x) = \mathrm{Taylor}(f(x), a)$ for <i>any</i> $a$, the function is <b>entire</b></li>
						</ul>
					</section>

					<section>
						<h2>Taylor series of <tt>exp(x)</tt></h2>
						<p>
						<img src="exp-taylor.svg" style="width: 70%"/>
						</p>
						<ul>
							<li>Which $N$ to take?</li>
							<li>What is the error? 
								<ul>
									<li>$|\mathrm{Taylor}_4(exp(2), 0)-exp(2)|\approx 0.002$</li>
									<li>$|\mathrm{Taylor}_4(exp(4), 0)-exp(4)|\approx 1.2$</li>
								</ul>
							</li>
						</ul>
					</section>

					<section>
						<h2>Remez algorithm</h2>
						<img src="Remesdemo.png" style="float: right; width: 35%">
						<ol style="width: 50%">
							<li>Find optimal polynomial: $P(x)$, for $x_1, x_2, ..., 	x_{N+2}$:
								$$P(x_1) - f(x_1) = +\varepsilon$$
								$$P(x_2) - f(x_2) = -\varepsilon$$
								$$...$$
							</li>
							<li>Move $x_i$ such that error $\le \varepsilon$ for any $x_1 \le x \le x_{N+2}$</li>
						</ol>
					</section>

				</section>

				<section>
					<h2>Application examples</h2>
					<a href="https://colab.research.google.com/drive/1PAXmH_v6sQmbmAYrRK48eCfFQWLcQO3K" target="_blank">
  <img width="400px" src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/>
</a>
				</section>

            </div>

            <script src="lib/js/head.min.js"></script>
            <script src="js/reveal.js"></script>

            <script>
                // More info about config & dependencies:
                // * https://github.com/hakimel/reveal.js#configuration
                // * https://github.com/hakimel/reveal.js#dependencies
                Reveal.initialize({
                    transition: 'fade',
                    math: {
                        mathjax: 'MathJax/MathJax.js',
                        config: 'TeX-AMS_HTML-full'
                    },
                    dependencies: [
                        { src: 'plugin/markdown/marked.js' },
                        { src: 'plugin/markdown/markdown.js' },
                        { src: 'plugin/notes/notes.js', async: true },
                        { src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
                        { src: 'plugin/math/math.js', async: true }
                    ]
                });
            </script>
    </body>
</html>
